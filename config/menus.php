<?php


return [





    //"ControllerNamespace" => "\\App\\LaravelMenuManager\\Controllers",
    "cacheTimeout" => 120,
    "contentSection" => "content",
    "scriptSection" => "scripts",
    "styleSection" => "styles",
    "layoutBladeFilePath" => "_layout",
    "ControllerNamespace" => "\\App\\Http\\Controllers\\",
    'cacheGroups' => [],
    "menus" => [

        [
            "imp" => \App\PageExtended::class,
            "enum" => \App\Enum\TableEnum::_PAGE,
            "title" => "Sayfa",
            "supplier" => \App\LaraveMenuManagerImplementations\PageSupplier::class
        ],

    ],


//    "menus" => [
//        [
//            "imp" => \App\LaravelMenuManager\MenuModels\IMenuModel::class,
//            "enum" => \App\LaravelMenuManager\MenuEnums::_TYPE_PAGE,
//            "title" => "Sayfa",
//            "supplier" => \App\LaravelMenuManager\Suppliers\ISupplier::class
//        ],
//    ],






];
