<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 18.01.2019
 * Time: 15:23
 */

namespace App;


use App\Context\Context;
use App\LaravelMenuManager\Models\LaravelMenuManagerItem;
use App\LaravelMenuManager\Models\LaravelMenuManagerItemExtended;

class LaravelMenuItemImplemented extends LaravelMenuManagerItemExtended
{
    public function getMenuPanelTitle()
    {
        return $this->getTranslated(Context::get()->defaultLanguage())->title;
    }

    public function getMenuFrontTitle()
    {
        return $this->getTranslated(Context::get()->defaultLanguage())->title;
    }

    public function getMenuFrontLink()
    {
        return $this->getTranslated(Context::get()->defaultLanguage())->link;
    }


}