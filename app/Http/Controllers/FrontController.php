<?php

namespace App\Http\Controllers;

use App\Context\Context;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class FrontController extends Controller
{

    public function __construct()
    {


        View::share([

            "defaultLanguage" => Context::get()->defaultLanguage()

        ]);

    }

}
