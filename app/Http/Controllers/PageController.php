<?php

namespace App\Http\Controllers;

use App\PageExtended;
use App\Repository\PageRepository;
use Illuminate\Http\Request;

class PageController extends FrontController
{


    public $pageRepository;
    public function __construct(Request $request)
    {
        parent::__construct();
        $this->pageRepository = new PageRepository();

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [

            "isCreate" => false,
            "page" => new PageExtended(),

        ];

        return view("edit")->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->pageRepository->updateOrInsert($request);
        return redirect()->route("pages.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data = [

            "isCreate" => true,
            "page" => $this->pageRepository->getByIdOrNew($id),

        ];

        return view("edit")->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->pageRepository->updateOrInsert($request,$id);
        return redirect()->route("pages.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->pageRepository->delete($id);
        return redirect()->route("pages.index");
    }
}
