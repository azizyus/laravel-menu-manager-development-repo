<?php

namespace App\Http\Controllers;

use App\Context\Context;
use App\LaravelMenuManager\Controllers\Traits\MenuBuilderControllerAsTrait;
use App\LaravelMenuManager\Factory\MenuFactory;
use App\LaravelMenuManager\MenuManager;
use App\LaravelMenuManager\Repository\MenuRepositoryDefault;
use Azizyus\LaravelLanguageHelper\App\Repositories\Eloquent\LanguageRepository;
use App\Repository\MenuManagerRepository;
use Illuminate\Http\Request;

class MenuBuilderController extends AdminController
{

    use MenuBuilderControllerAsTrait;
    public function __construct()
    {

        $menuManager = new MenuManager(new MenuManagerRepository());
        $menuManager->setDefaultPanelLanguage(Context::get()->defaultLanguage());
        $this->__constructTrait($menuManager,new LanguageRepository(),new MenuFactory());

    }


}
