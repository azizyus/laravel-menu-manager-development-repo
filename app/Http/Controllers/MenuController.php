<?php

namespace App\Http\Controllers;

use App\Context\Context;
use App\Repository\MenuManagerRepository;
use Illuminate\Http\Request;

class MenuController extends FrontController
{

    public $menuRepository;
    public function __construct()
    {
        parent::__construct();
        $this->menuRepository = new MenuManagerRepository();
    }

    public function index()
    {

        $data = [

            "menus" => $this->menuRepository->allCacheOutput(Context::get()->frontLanguage()),

        ];


        return view("menu")->with($data);
    }

    public function recursive()
    {


        $data = [

            "menus" => $this->menuRepository->allCacheOutput(Context::get()->frontLanguage()),

        ];


        return view("menu-recursive")->with($data);
    }

}
