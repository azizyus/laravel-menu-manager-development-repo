<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 18.01.2019
 * Time: 14:24
 */

namespace App\Repository;


use Azizyus\LaravelLanguageHelper\App\Models\Language;
use App\Page;
use App\PageExtended;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PageRepository
{

    public function updateOrInsert(Request $request,$id=null)
    {

        $page = PageExtended::find($id);

        if(!$page)  $page = new PageExtended();
        $page->save();
        foreach (Language::all() as $l)
        {

            $page->updateTranslation($l,[

                "title" => $request->get("title$l->id"),
                "description" => $request->get("description$l->id"),
                "slug" => Str::slug($request->get("title$l->id"))

            ]);

        }




    }


    public function baseQuery() : Page
    {
        return new PageExtended();
    }

    public function getByIdOrNew($id) : ?PageExtended
    {
        $page = $this->baseQuery()->where("id",$id)->first();
        if(!$page) $page = new PageExtended();
        return $page;
    }

    public function delete($id)
    {

        $page = Page::find($id);

        if($page)
        {
            $page->delete();
        }

    }

}
