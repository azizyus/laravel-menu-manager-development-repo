<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 18.01.2019
 * Time: 15:22
 */

namespace App\Repository;


use App\LaravelMenuItemImplemented;
use App\LaravelMenuManager\Models\LaravelMenuManagerItemExtended;
use App\LaravelMenuManager\Repository\MenuRepository;

class MenuManagerRepository extends MenuRepository
{
    public function baseModel(): LaravelMenuManagerItemExtended
    {
        return  new LaravelMenuItemImplemented();
    }


}