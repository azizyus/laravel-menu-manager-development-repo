<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 3/15/19
 * Time: 1:21 AM
 */

namespace App\Repository;


class LanguageRepository extends \Azizyus\LaravelLanguageHelper\App\Repositories\Eloquent\LanguageRepository
{

    public function getDefaultLanguage()
    {
        return $this->baseQuery()->where("isDefault",true)->first();
    }

    public function getFrontLanguage()
    {
        return $this->baseQuery()->orderBy("id","DESC")->first();
    }

}
