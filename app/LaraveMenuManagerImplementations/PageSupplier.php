<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 18.01.2019
 * Time: 15:38
 */

namespace App\LaraveMenuManagerImplementations;


use App\LaravelMenuManager\Suppliers\ISupplier;
use App\Repository\PageRepository;

class PageSupplier implements ISupplier
{

    public $pageRepository;
    public function __construct()
    {
        $this->pageRepository = new PageRepository();
    }

    public function vomit(): array
    {

        $pages = $this->pageRepository->baseQuery()->all()->mapInto(PageSupplierItem::class);
        return $pages->all();

    }

}