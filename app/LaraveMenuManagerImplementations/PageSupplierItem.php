<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 18.01.2019
 * Time: 15:39
 */

namespace App\LaraveMenuManagerImplementations;


use App\LaravelMenuManager\Suppliers\SupplierResults\ISupplierItem;
use App\PageExtended;

class PageSupplierItem implements ISupplierItem
{

    public $page;
    public function __construct(PageExtended $page)
    {
        $this->page = $page;
    }

    public function title(): String
    {
        return $this->page->panelTranslate()->title;
    }

    public function id(): Int
    {
        return $this->page->id;
    }


}