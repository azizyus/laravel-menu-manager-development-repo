<?php

namespace App;

use App\Context\Context;
use App\Enum\TableEnum;
use App\LaravelMenuManager\Cache\IMenuCacheable;
use App\LaravelMenuManager\Delete\IDeleteEnum;
use App\LaravelMenuManager\MenuModels\IMenuModel;
use Azizyus\LaravelLanguageHelper\App\Models\Traits\HasLanguageProperties;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $created_at
 * @property string $updated_at
 */
class PageExtended extends Page implements IMenuModel, IMenuCacheable,IDeleteEnum
{
    protected $table = "pages";
    use HasLanguageProperties;

    public function getLanguageProperties()
    {
        return [
            "title",
            "description",
            "slug"
        ];
    }

    public function getDeleteEnum(): Int
    {
        return $this->translateEnum();
    }

    public function getMenuPanelTitle()
    {
        return $this->panelTranslate()->title;
    }

    public function getFrontTitle()
    {
        return $this->frontTranslate()->title;
    }

    public function getFrontRoute()
    {
        return route("pages.detail",["slug"=>$this->frontTranslate()->slug]);
    }

    public function cacheOutput()
    {
        return [

            "title"=>$this->getFrontTitle(),
            "link" => $this->getFrontRoute(),

        ];
    }

    public function panelTranslate()
    {
        return $this->getTranslated(Context::get()->defaultLanguage());
    }

    public function frontTranslate()
    {
        return $this->getTranslated(Context::get()->frontLanguage());
    }

    public function translateEnum()
    {
        return TableEnum::_PAGE;
    }

}
