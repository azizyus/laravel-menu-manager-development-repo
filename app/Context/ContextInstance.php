<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 18.01.2019
 * Time: 14:47
 */

namespace App\Context;


use Azizyus\LaravelLanguageHelper\App\Models\ILanguage;
use Azizyus\LaravelLanguageHelper\App\Models\Language;
use App\Repository\LanguageRepository;

class ContextInstance
{

    public $languageRepository;
    public function __construct()
    {
        $this->languageRepository = new LanguageRepository();
    }


    public function defaultLanguage() : ILanguage
    {
        return $this->languageRepository->getDefaultLanguage();
    }

    public function frontLanguage() : ILanguage
    {
        return $this->languageRepository->getFrontLanguage();
    }

}
