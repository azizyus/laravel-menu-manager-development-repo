<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 18.01.2019
 * Time: 14:46
 */

namespace App\Context;


class Context
{

    public static $context = null;
    public static function get()
    {

        if(self::$context == null)
        {
            self::$context = new ContextInstance();
        }

        return self::$context;
    }

}