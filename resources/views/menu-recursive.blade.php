<ul>
    @foreach($menus as $menu)
        <li>
            <a target="_blank" href="{{$menu["link"]}}">{{$menu["title"]}}</a>
            @if($menu["childs"])
                @include("menu-recursive",["menus"=>$menu["childs"]])
            @endif
        </li>
    @endforeach
</ul>

