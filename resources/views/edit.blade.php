
@if($isCreate)
    <form action="{{route("pages.update",["id"=>$page->id])}}" method="POST">
        {{method_field("PUT")}}
@else
            <form action="{{route("pages.store")}}" method="POST">
@endif
    {{csrf_field()}}

    @foreach((new \App\Repository\LanguageRepository())->getAll()  as $language)

                    <div class="form-group">
                        <label for="title{{$language->id}}">Title ({{$language->shortTitle}})</label>
                        <input value="{{$page->getTranslated($language)->title}}" type="text{{$language->id}}" name="title{{$language->id}}" id="title{{$language->id}}">
                    </div>


                    <div class="form-group">
                        <label for="description{{$language->id}}">Description</label>
                        <textarea name="description{{$language->id}}" id="description{{$language->id}}">{{$page->getTranslated($language)->description}}</textarea>
                    </div>
    @endforeach
                <br>
                <input type="submit" value="SUBMIT">
</form>
