<ul>
    @foreach($menus as $menu)
        <li>
                <a target="_blank" href="{{$menu["link"]}}">{{$menu["title"]}}</a>
                @foreach($menu["childs"] as $child)
                        <ul>
                                <li>
                                        <a target="_blank" href="{{$child["link"]}}">{{$child["title"]}}</a>
                                        @foreach($child["childs"] as $subChilds)
                                                <ul>
                                                        <li>
                                                                <a target="_blank" href="{{$subChilds["link"]}}">{{$subChilds["title"]}}</a>
                                                        </li>
                                                </ul>
                                        @endforeach
                                </li>
                        </ul>
                @endforeach
        </li>
    @endforeach
</ul>